package com.citi.trading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/accounts")
public class BrokerService {
	
	@Autowired
	public ApplicationContext context;
	
	private int id;
	
	private Map<Integer, Investor> allInvestors = new HashMap<>();
	
//	@Value("#{'${com.citi.trading.BrokerService.STOCK}'.split(',')}")
//	private List<String> stockList;
//	
//	@Value("#{'${com.citi.trading.BrokerService.PRICE}'.split(',')}")
//	private List<Integer> priceList;
//	
	public BrokerService() {
		
	}
	
	
	@PostConstruct
	public void init() {
		Investor investor = context.getBean(Investor.class);
		Map<String, Integer> portfolio = new HashMap<>();
		
		portfolio.put("APP", 100);
		investor.setPortfolio(portfolio);
		allInvestors.put(1, investor);
		

		Investor investor1 = context.getBean(Investor.class);
		Map<String, Integer> portfolio2 = new HashMap<>();
		
		portfolio2.put("GOOG", 60);
		investor1.setPortfolio(portfolio2);
		allInvestors.put(2, investor1);
		id = 2;
	}

	@GetMapping()
	public List<Investor> getAll() {
		return new ArrayList<Investor>(allInvestors.values());
	}
	
	@RequestMapping(value="/{ID}",method=RequestMethod.GET)
	public ResponseEntity<String> getByID(@PathVariable("ID") int ID) {
		System.out.println ("GET ID=" + ID);
        Investor investor = allInvestors.get(ID);
        
        return investor != null
            ? new ResponseEntity<String> (String.valueOf(investor), HttpStatus.OK)
            : new ResponseEntity<String> ("", HttpStatus.NOT_FOUND);
	}
	
//	public Investor openAccount(double cash) {
//		Investor investor = context.getBean(Investor.class);
//		investor.setCash(cash);
//		id++;
//		allInvestors.put(id, investor);
//		return investor;
//		
//	}
//    */
    @RequestMapping(value="/{cash}",method=RequestMethod.POST)
    public ResponseEntity<String> openAccount
    (
        @PathVariable("cash") int cash
    )
    {
//        System.out.println ("POST cash=" + cash + "&value=" + value);
        Investor investor = context.getBean(Investor.class);
        investor.setCash(cash);
        id++;
        allInvestors.put(id, investor);
        return new ResponseEntity<String> (String.valueOf(investor), HttpStatus.CREATED);
        
    }
	
	public void closeAccount(int ID) {
		allInvestors.remove(ID);
	}
	

}
