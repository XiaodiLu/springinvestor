package com.citi.trading;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.PropertySource;



@SpringBootApplication
@PropertySource("classpath:application.properties")
public class BrokerApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(BrokerApplication.class, args);
	}
//	public static void main(String[] args) {
//		try (AnnotationConfigApplicationContext context = 
//				new AnnotationConfigApplicationContext(BrokerApplication.class);) {
//			
//			BrokerService aObject = context.getBean(BrokerService.class);
//			System.out.println(aObject.getByID(0).getCash());
//		}
//	
//	}
}
