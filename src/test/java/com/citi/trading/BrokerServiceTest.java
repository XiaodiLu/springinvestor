package com.citi.trading;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.InvestorTest.MockMarket;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=BrokerServiceTest.Config.class)
@PropertySource("classpath:application.properties")
@DirtiesContext(classMode=ClassMode.BEFORE_EACH_TEST_METHOD)
public class BrokerServiceTest {

	@Autowired
	private BrokerService bs;
	
	@Configuration
	public static class Config{
	
		@Bean 
		public static BrokerService brokerService() {
			BrokerService bs = new BrokerService();
			return bs;
		}
		
		@Bean
		public static MockMarket mockMarket() {
			MockMarket market = new MockMarket();
			return market;
		}
		
		@Bean
		@Scope("prototype")
		public static Investor investor() {
			Investor investor = new Investor();
			return investor;
		}
	
	}
	
//	@Test
//	public void testGetByID() {
//		Investor investor = bs.getByID(1);
//		assertThat(investor.getPortfolio(), hasEntry(equalTo("APP"), equalTo(100)));
//		Investor investor2 = bs.getByID(2);
//		assertThat(investor2.getPortfolio(), hasEntry(equalTo("GOOG"), equalTo(60)));
//	}
	
	@Test
	public void testGetAll() {
		List<Investor> investors = bs.getAll();
		assertThat(investors.size(), equalTo(2));
	}
	
//	@Test
//	public void testOpenAccount() {
//		Investor investor = bs.openAccount(1000);
//		assertThat(investor.getCash(), equalTo(1000.0));
//	}
//
//	
//	@Test
//	public void testCloseAccount() {
//		Investor investor = bs.getByID(1);
//		assertThat(investor.getPortfolio(), hasEntry(equalTo("APP"), equalTo(100)));
//		bs.closeAccount(1);
//		List<Investor> investors = bs.getAll();
//		Investor investor2 = bs.getByID(2);
//		assertThat(investor2.getPortfolio(), hasEntry(equalTo("GOOG"), equalTo(60)));
//		assertThat(investors.size(), equalTo(1));
//	}
}

